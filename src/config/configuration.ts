export default () => ({
  jwt: {
    secret: process.env.JWT_SECRET || 'secretKey',
    saltRounds: parseInt(process.env.SALT_ROUNDS, 10) || 10,
  },
});