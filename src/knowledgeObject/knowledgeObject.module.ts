import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { KnowledgeObject } from './knowledgeObject.entity';
import { KnowledgeObjectController } from './knowledgeObject.controller';
import { KnowledgeObjectService } from './knowledgeObject.service';

@Module({
  imports: [TypeOrmModule.forFeature([KnowledgeObject])],
  controllers: [KnowledgeObjectController],
  providers: [KnowledgeObjectService],
  exports: [KnowledgeObjectService],
})
export class KnowledgeObjectModule {}
