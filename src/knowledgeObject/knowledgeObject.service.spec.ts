import { Test, TestingModule } from '@nestjs/testing';
import { KnowledgeObjectService } from './knowledgeObject.service';
import { Repository } from 'typeorm';
import { KnowledgeObject } from './knowledgeObject.entity';
import { getRepositoryToken } from '@nestjs/typeorm';

describe('KnowledgeObjectService', () => {
  let service: KnowledgeObjectService;
  let repository: Repository<KnowledgeObject>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        KnowledgeObjectService,
        { provide: getRepositoryToken(KnowledgeObject), useClass: Repository },
      ],
    }).compile();

    service = module.get<KnowledgeObjectService>(KnowledgeObjectService);
    repository = module.get(getRepositoryToken(KnowledgeObject));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
