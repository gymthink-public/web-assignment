import { Controller, Get, Put, Delete, Request, Query, Body, UseGuards } from '@nestjs/common';
import { KnowledgeObjectService } from './knowledgeObject.service';
import { KnowledgeObjectDto } from './dto/knowledgeObject.dto';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { ApiTags, ApiBearerAuth, ApiBody } from '@nestjs/swagger';
import { IdDto } from './dto/id.dto';
import { ObjectID } from 'mongodb';

@ApiTags('knowledgeObject')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('knowledgeObject')
export class KnowledgeObjectController {
  constructor(private knowledgeObjectService: KnowledgeObjectService) {}

  @Get()
  async getAll(@Request() req) {
    return this.knowledgeObjectService.getAll(req.user.id);
  }

  @ApiBody({ type: KnowledgeObjectDto })
  @Put()
  async update(@Request() req, @Body() knowledgeObjectDto: KnowledgeObjectDto) {
    return this.knowledgeObjectService.update(req.user.id, knowledgeObjectDto);
  }

  @Delete()
  async delete(@Request() req, @Query() query: IdDto) {
    return this.knowledgeObjectService.delete(req.user.id, new ObjectID(query.id));
  }
}
