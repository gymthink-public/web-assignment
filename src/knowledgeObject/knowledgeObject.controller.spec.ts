import { Test, TestingModule } from '@nestjs/testing';
import { KnowledgeObjectController } from './knowledgeObject.controller';
import { KnowledgeObjectService } from './knowledgeObject.service';

describe('Knowledge Object Controller', () => {
  let controller: KnowledgeObjectController;
  let knowledgeObjectService: KnowledgeObjectService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [KnowledgeObjectController],
      providers: [{ provide: KnowledgeObjectService, useValue: {} }],
    }).compile();

    controller = module.get<KnowledgeObjectController>(KnowledgeObjectController);
    knowledgeObjectService = module.get<KnowledgeObjectService>(KnowledgeObjectService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
