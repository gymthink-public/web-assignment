import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ObjectID } from 'mongodb';
import { KnowledgeObject } from './knowledgeObject.entity';
import { KnowledgeObjectDto } from './dto/knowledgeObject.dto';
import { CreateKnowledgeObjectDto } from './dto/createKnowledgeObject.dto';

@Injectable()
export class KnowledgeObjectService {
  constructor(
    @InjectRepository(KnowledgeObject)
    private knowledgeObjectRepository: Repository<KnowledgeObject>,
  ) {}

  async create(
    userId: ObjectID,
    createKnowledgeObjectDto: CreateKnowledgeObjectDto,
  ) {
    const knowledgeObject = new KnowledgeObject({
      userId,
      title: createKnowledgeObjectDto.title,
      knowledge: createKnowledgeObjectDto.knowledge,
    });
    return await this.knowledgeObjectRepository.save(knowledgeObject);
  }

  async getAll(userId: ObjectID) {
    return await this.knowledgeObjectRepository.find({ userId });
  }

  async update(userId: ObjectID, knowledgeObjectDto: KnowledgeObjectDto) {
    const knowledgeObject: KnowledgeObject = await this.knowledgeObjectRepository.findOne(
      knowledgeObjectDto.id,
    );
    if (!knowledgeObject) {
      throw new BadRequestException('KnowledgeObject could not found!');
    }

    if (!knowledgeObject.userId.equals(userId)) {
      throw new BadRequestException('KnowledgeObject does not belong to you!');
    }

    knowledgeObject.title = knowledgeObjectDto.title;
    knowledgeObject.knowledge = knowledgeObjectDto.knowledge;

    return await this.knowledgeObjectRepository.save(knowledgeObject);
  }

  async delete(userId: ObjectID, knowledgeObjectId: ObjectID) {
    const knowledgeObject: KnowledgeObject = await this.knowledgeObjectRepository.findOne(
      knowledgeObjectId.toHexString(),
    );
    if (!knowledgeObject) {
      throw new BadRequestException('KnowledgeObject could not found!');
    }

    if (!knowledgeObject.userId.equals(userId)) {
      throw new BadRequestException('KnowledgeObject does not belong to you!');
    }

    return await this.knowledgeObjectRepository.delete(knowledgeObject);
  }
}
