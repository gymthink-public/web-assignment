import { Entity, Column, ObjectIdColumn } from 'typeorm';
import { ObjectID } from 'mongodb';

@Entity()
export class KnowledgeObject {
  @ObjectIdColumn()
  id: ObjectID;

  @Column()
  userId: ObjectID;

  @Column()
  title: string;

  @Column()
  knowledge: string;

  constructor(param) {
    Object.assign(this, param);
  }
}
