import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { EpoxModule } from './epox/epox.module';
import { KnowledgeObjectModule } from './knowledgeObject/knowledgeObject.module';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { join } from 'path';
import { MailerModule } from '@nestjs-modules/mailer';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';
import configuration from './config/configuration';

@Module({
  imports: [
    AuthModule,
    UsersModule,
    EpoxModule,
    KnowledgeObjectModule,
    ConfigModule.forRoot({
      load: [configuration],
      isGlobal: true,
    }),
    TypeOrmModule.forRoot({
      type: 'mongodb',
      url: 'mongodb+srv://mongouser:jHKeERQ79+@burdayimdb.mp5ml.mongodb.net/epox',
      entities: [join(__dirname, '**/**.entity{.ts,.js}')],
      synchronize: true,
      useNewUrlParser: true,
      logging: true,
      autoLoadEntities: true,
    }),
    MailerModule.forRoot({
      transport: {
        host: 'smtp.yandex.com',
        port: 587,
        secure: false,
        auth: {
          user: "noreply@epox.app",
          pass: "aauadxeuykfkkwde",
        },
      },
      defaults: {
        from:'"epox" <noreply@epox.app>',
      },
      template: {
        dir: __dirname + '/templates',
        adapter: new HandlebarsAdapter(),
        options: {
          strict: true,
        },
      },
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
