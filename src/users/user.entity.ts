import { Entity, Column, ObjectIdColumn } from 'typeorm';
import { ObjectID } from 'mongodb';

@Entity()
export class User {
  @ObjectIdColumn()
  id: ObjectID;

  @Column()
  email: string;

  @Column()
  password: string;

  @Column()
  fullName: string;

  @Column()
  birthDate: string;

  @Column()
  gender: string;

  @Column()
  verified: boolean;

  @Column()
  verificationCode: string;

  @Column()
  verificationCodeExpirationDate: Date;

  @Column()
  resetPasswordCode: string;

  @Column()
  resetPasswordCodeExpirationDate: Date;

  constructor(param) {
    Object.assign(this, param);
  }
}
