import { Controller, Get, Request, UseGuards } from '@nestjs/common';
import { UsersService } from './users.service';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { ApiTags, ApiBearerAuth } from '@nestjs/swagger';

@ApiTags('users')
@ApiBearerAuth()
@Controller('users')
export class UsersController {
  constructor(private usersService: UsersService) {}

  @UseGuards(JwtAuthGuard)
  @Get('profile')
  async getProfile(@Request() req) {
    const {
      password,
      verificationCode,
      verified,
      verificationCodeExpirationDate,
      resetPasswordCode,
      resetPasswordCodeExpirationDate,
      ...result
    } = await this.usersService.findOne(req.user.id);

    return result;
  }
}
