import { Test, TestingModule } from '@nestjs/testing';
import { UsersService } from './users.service';
import { Repository } from 'typeorm';
import { User } from './user.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { ObjectID } from 'mongodb';

describe('UsersService', () => {
  let service: UsersService;
  let repository: Repository<User>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UsersService,
        { provide: getRepositoryToken(User), useClass: Repository },
      ],
    }).compile();

    service = module.get<UsersService>(UsersService);
    repository = module.get(getRepositoryToken(User));
  });

  it('given user id when findOne called then it should return user with respected id', async () => {
    // given
    const id = new ObjectID(1);
    const expectedUser = new User({
      id: id,
      email: 'john@doe.com',
      password: '1234password',
      birthDate: '1/10/1987',
      fullName: 'John Doe',
      gender: 'male',
    });

    const repositoryFindOneSpy = jest
      .spyOn(repository, 'findOne')
      .mockResolvedValue(expectedUser);

    // when
    const actualUser = await service.findOne(id);

    // then
    expect(repositoryFindOneSpy).toBeCalledWith(id.toHexString());
    expect(actualUser).toEqual(actualUser);
  });

  it('given email when findOneByEmail called then it should return user with respected email', async () => {
    const email = 'john@doe.com';
    const expectedUser = new User({
      id: new ObjectID(1),
      email: email,
      password: '1234password',
      birthDate: '1/10/1987',
      fullName: 'John Doe',
      gender: 'male',
    });

    const repositorySpy = jest
      .spyOn(repository, 'findOne')
      .mockResolvedValue(expectedUser);

    const actualUser = await service.findOneByEmail(email);

    expect(repositorySpy).toBeCalledWith({ email });
    expect(actualUser).toEqual(expectedUser);
  });

  it('given user when insertOne called then repository.insert should be called with user', async () => {
    // given
    const user = new User({
      id: new ObjectID(1),
      email: 'john@doe.com',
      password: '1234password',
      birthDate: '1/10/1987',
      fullName: 'John Doe',
      gender: 'male',
    });

    const repositorySpy = jest.spyOn(repository, 'insert').mockImplementation();

    // when
    await service.insertOne(user);

    // then
    expect(repositorySpy).toBeCalledWith(user);
  });
});
