import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { User } from './user.entity';

describe('Users Controller', () => {
  let controller: UsersController;
  let service: UsersService;
  let repositoryMock: Repository<User>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [
        UsersService,
        {
          provide: getRepositoryToken(User),
          useClass: Repository,
        },
      ],
    }).compile();

    controller = module.get<UsersController>(UsersController);
    service = module.get<UsersService>(UsersService);
    repositoryMock = module.get(getRepositoryToken(User));
  });

  describe('getProfile', () => {
    it('given user object from token when getProfile called then returns profile', async () => {
      // given
      const userFromToken = { id: 1, email: 'test@epox.app' };
      const userFromService = new User({
        id: 1,
        email: 'test@epox.app',
        password: 'myPassw0rD',
        fullName: 'test',
        birthDate: '01/01/1970',
        gender: 'Male',
      });
      const req = { user: userFromToken };

      jest
        .spyOn(service, 'findOne')
        .mockImplementation(async id => userFromService);

      // when
      const result = await controller.getProfile(req);

      // then
      const expectedResult = {
        id: userFromService.id,
        email: userFromService.email,
        fullName: userFromService.fullName,
        birthDate: userFromService.birthDate,
        gender: userFromService.gender,
      };

      expect(result).toStrictEqual(expectedResult);
    });
  });
});
