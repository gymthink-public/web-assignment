import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty } from 'class-validator';
import { User } from '../../users/user.entity';

export class SignUpDto {
  @ApiProperty()
  @IsEmail()
  email: string;

  @ApiProperty()
  @IsNotEmpty()
  password: string;

  @ApiProperty()
  @IsNotEmpty()
  fullName: string;

  @ApiProperty()
  @IsNotEmpty()
  birthDate: string;

  @ApiProperty()
  @IsNotEmpty()
  gender: string;

  public toUser(): User {
    return new User({
      email: this.email,
      password: this.password,
      fullName: this.fullName,
      birthDate: this.birthDate,
      gender: this.gender,
    });
  }
}
