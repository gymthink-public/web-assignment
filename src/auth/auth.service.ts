import {
  Injectable,
  UnauthorizedException,
  BadRequestException,
  ConflictException,
  ForbiddenException,
} from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';

import * as dayjs from 'dayjs';
const bcrypt = require('bcryptjs');

import { UsersService } from '../users/users.service';
import { LoginDto } from './dto/login.dto';
import { SignUpDto } from './dto/signUp.dto';
import { VerifyUserDto } from './dto/verifyUser.dto';
import { User } from '../users/user.entity';
import { EmailDto } from './dto/email.dto';
import { ResetPasswordDto } from './dto/resetPassword.dto';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
    private mailerService: MailerService,
    private configService: ConfigService,
  ) {}

  isPasswordMatching(passwordToCheck: string, passwordHash: string): boolean {
    return bcrypt.compare(passwordToCheck, passwordHash);
  }

  async login(loginDto: LoginDto) {
    const user: User = await this.usersService.findOneByEmail(loginDto.email);
    if (!user) {
      throw new UnauthorizedException('E-mail or password is wrong!');
    }

    const isPasswordMatching = await this.isPasswordMatching(loginDto.password, user.password);
    if (!isPasswordMatching) {
      throw new UnauthorizedException('E-mail or password is wrong!');
    }

    if (!user.verified) {
      throw new ForbiddenException('Verify your email first!');
    }

    const payload = { email: user.email, sub: user.id.toHexString() };
    return {
      access_token: this.jwtService.sign(payload),
    };
  }

  async signUp(signUpDto: SignUpDto) {
    const existedUser: User = await this.usersService.findOneByEmail(signUpDto.email);
    if (existedUser) {
      throw new ConflictException('Email already exists');
    }

    const verificationCode = generateRandomString();
    const verificationCodeExpirationDate = dayjs().add(1, 'day');

    const saltRounds = this.configService.get<number>('jwt.saltRounds');

    const newUser: User = signUpDto.toUser();
    newUser.password = await bcrypt.hash(newUser.password, saltRounds)
    newUser.verified = false;
    newUser.verificationCode = verificationCode;
    newUser.verificationCodeExpirationDate = verificationCodeExpirationDate.toDate();
    newUser.resetPasswordCode = null;
    newUser.resetPasswordCodeExpirationDate = null;

    await this.usersService.insertOne(newUser);
    await this.sendVerificationMail(newUser.email, verificationCode);
  }

  async verifyUser(verifyDto: VerifyUserDto) {
    const user: User = await this.usersService.findOneByEmail(verifyDto.email);
    if (!user) {
      throw new BadRequestException('Email could not found!');
    }

    if (user.verified) {
      throw new BadRequestException('Already Verified!');
    }

    if (dayjs().isAfter(dayjs(user.verificationCodeExpirationDate))) {
      throw new BadRequestException('Code expired!');
    }

    if (user.verificationCode !== verifyDto.code) {
      throw new BadRequestException('Code mismatch!');
    }

    user.verified = true;
    user.verificationCode = null;
    user.verificationCodeExpirationDate = null;

    await this.usersService.save(user);
  }

  async sendVerificationCode(emailDto: EmailDto) {
    const user: User = await this.usersService.findOneByEmail(emailDto.email);
    if (!user) {
      throw new BadRequestException('Email could not found!');
    }

    if (user.verified) {
      throw new BadRequestException('Already Verified!');
    }

    const verificationCode: string = generateRandomString();
    const verificationCodeExpirationDate: dayjs.Dayjs = dayjs().add(1, 'day');

    user.verificationCode = verificationCode;
    user.verificationCodeExpirationDate = verificationCodeExpirationDate.toDate();

    await this.usersService.save(user);
    await this.sendVerificationMail(user.email, verificationCode);
  }

  async sendResetPasswordCode(emailDto: EmailDto) {
    const user: User = await this.usersService.findOneByEmail(emailDto.email);
    if (!user) {
      throw new BadRequestException('Email could not found!');
    }

    const resetPasswordCode: string = generateRandomString();
    const resetPasswordCodeExpirationDate: dayjs.Dayjs = dayjs().add(1, 'day');

    user.resetPasswordCode = resetPasswordCode;
    user.resetPasswordCodeExpirationDate = resetPasswordCodeExpirationDate.toDate();

    await this.usersService.save(user);
    await this.sendResetPasswordMail(user.email, resetPasswordCode);
  }

  async resetPassword(resetPasswordDto: ResetPasswordDto) {
    const user: User = await this.usersService.findOneByEmail(resetPasswordDto.email);
    if (!user) {
      throw new BadRequestException('Email could not found!');
    }

    if (dayjs().isAfter(dayjs(user.resetPasswordCodeExpirationDate))) {
      throw new BadRequestException('Code expired!');
    }

    if (user.resetPasswordCode !== resetPasswordDto.code) {
      throw new BadRequestException('Code mismatch!');
    }

    const saltRounds = this.configService.get<number>('jwt.saltRounds');

    user.password = await bcrypt.hash(resetPasswordDto.password, saltRounds)
    user.resetPasswordCode = null;
    user.resetPasswordCodeExpirationDate = null;

    await this.usersService.save(user);
  }

  async sendVerificationMail(email: string, code: string) {
    return this.mailerService.sendMail({
      to: email,
      subject: 'Welcome to Epox - Email Verification',
      template: 'emailVerificationMail',
      context: { email, code },
    });
  }

  async sendResetPasswordMail(email: string, code: string) {
    return this.mailerService.sendMail({
      to: email,
      subject: 'Epox - Reset Password',
      template: 'passwordResetMail',
      context: { email, code },
    });
  }
}

function generateRandomString(length = 6) {
  return Math.random()
    .toString(20)
    .substr(2, length);
}
