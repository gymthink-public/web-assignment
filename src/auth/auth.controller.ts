import { Controller, Get, Post, Body, Query } from '@nestjs/common';
import { AuthService } from './auth.service';
import { LoginDto } from './dto/login.dto';
import { SignUpDto } from './dto/signUp.dto';
import { EmailDto } from './dto/email.dto';
import { ResetPasswordDto } from './dto/resetPassword.dto';
import { ApiTags, ApiBody } from '@nestjs/swagger';
import { VerifyUserDto } from './dto/verifyUser.dto';

@ApiTags('auth')
@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @ApiBody({ type: LoginDto })
  @Post('login')
  async login(@Body() loginDto: LoginDto) {
    return this.authService.login(loginDto);
  }

  @ApiBody({ type: SignUpDto })
  @Post('sign-up')
  async signUp(@Body() signUpDto: SignUpDto) {
    return this.authService.signUp(signUpDto);
  }

  @Get('verify-user')
  async verifyUser(@Query() verifyUserDto: VerifyUserDto) {
    return this.authService.verifyUser(verifyUserDto).then(() => { return "Successfully verified!" });
  }

  @Post('send-verification-code')
  async sendVerificationCode(@Body() emailDto: EmailDto) {
    return this.authService.sendVerificationCode(emailDto);
  }

  @Post('send-reset-password-code')
  async sendResetPasswordCode(@Body() emailDto: EmailDto) {
    return this.authService.sendResetPasswordCode(emailDto);
  }

  @Post('reset-password')
  async resetPassword(@Body() resetPasswordDto: ResetPasswordDto) {
    return this.authService.resetPassword(resetPasswordDto);
  }
}
