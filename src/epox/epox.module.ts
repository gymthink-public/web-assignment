import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Epox } from './epox.entity';
import { EpoxController } from './epox.controller';
import { EpoxService } from './epox.service';
import { KnowledgeObjectModule } from '../knowledgeObject/knowledgeObject.module';

@Module({
  imports: [
    KnowledgeObjectModule,
    TypeOrmModule.forFeature([Epox]),
  ],
  controllers: [EpoxController],
  providers: [EpoxService]
})
export class EpoxModule {}
