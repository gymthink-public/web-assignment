import { Test, TestingModule } from '@nestjs/testing';
import { EpoxService } from './epox.service';
import { KnowledgeObjectService } from '../knowledgeObject/knowledgeObject.service';
import { Repository } from 'typeorm';
import { Epox } from './epox.entity';
import { getRepositoryToken } from '@nestjs/typeorm';

describe('EpoxService', () => {
  let service: EpoxService;
  let knowledgeObjectService: KnowledgeObjectService;
  let repository: Repository<Epox>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        EpoxService,
        { provide: getRepositoryToken(Epox), useClass: Repository },
        {
          provide: KnowledgeObjectService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<EpoxService>(EpoxService);
    knowledgeObjectService = module.get<KnowledgeObjectService>(KnowledgeObjectService);
    repository = module.get(getRepositoryToken(Epox));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
