import { Controller, Get, Post, Put, Delete, Request, Query, Body, UseGuards } from '@nestjs/common';
import { EpoxService } from './epox.service';
import { CreateEpoxDto } from './dto/createEpox.dto';
import { EpoxDto } from './dto/epox.dto';
import { ConvertEpoxDto } from './dto/convertEpox.dto';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { ApiTags, ApiBearerAuth, ApiBody } from '@nestjs/swagger';
import { IdDto } from './dto/id.dto';
import { ObjectID } from 'mongodb';

@ApiTags('epox')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('epox')
export class EpoxController {
  constructor(private epoxService: EpoxService) {}

  @ApiBody({ type: CreateEpoxDto })
  @Post()
  async create(@Request() req, @Body() createEpoxDto: CreateEpoxDto) {
    return this.epoxService.create(req.user.id, createEpoxDto);
  }

  @Get()
  async getAll(@Request() req) {
    return this.epoxService.getAll(req.user.id);
  }

  @ApiBody({ type: EpoxDto })
  @Put()
  async update(@Request() req, @Body() epoxDto: EpoxDto) {
    return this.epoxService.update(req.user.id, epoxDto);
  }

  @Delete()
  async delete(@Request() req, @Query() query: IdDto) {
    return this.epoxService.delete(req.user.id, new ObjectID(query.id));
  }

  @ApiBody({ type: ConvertEpoxDto })
  @Post("/convert")
  async convert(@Request() req, @Body() convertEpoxDto: ConvertEpoxDto) {
    return this.epoxService.convert(req.user.id, convertEpoxDto);
  }
}
