import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ObjectID } from 'mongodb';
import { Epox } from './epox.entity';
import { CreateEpoxDto } from './dto/createEpox.dto';
import { EpoxDto } from './dto/epox.dto';
import { ConvertEpoxDto } from './dto/convertEpox.dto';
import { KnowledgeObjectService } from '../knowledgeObject/knowledgeObject.service';
import { CreateKnowledgeObjectDto } from '../knowledgeObject/dto/createKnowledgeObject.dto';

@Injectable()
export class EpoxService {
  constructor(
    @InjectRepository(Epox)
    private epoxRepository: Repository<Epox>,
    private knowledgeObjectService: KnowledgeObjectService,
  ) {}

  async create(userId: ObjectID, createEpoxDto: CreateEpoxDto) {
    const epox = new Epox({ userId, title: createEpoxDto.title });
    return await this.epoxRepository.save(epox);
  }

  async getAll(userId: ObjectID) {
    return await this.epoxRepository.find({ userId });
  }

  async update(userId: ObjectID, epoxDto: EpoxDto) {
    const epox: Epox = await this.epoxRepository.findOne(epoxDto.id);
    if (!epox) {
      throw new BadRequestException('Epox could not found!');
    }

    if (!epox.userId.equals(userId)) {
      throw new BadRequestException('Epox does not belong to you!');
    }

    epox.title = epoxDto.title;

    return await this.epoxRepository.save(epox);
  }

  async delete(userId: ObjectID, epoxId: ObjectID) {
    const epox: Epox = await this.epoxRepository.findOne(epoxId.toHexString());
    if (!epox) {
      throw new BadRequestException('Epox could not found!');
    }

    if (!epox.userId.equals(userId)) {
      throw new BadRequestException('Epox does not belong to you!');
    }

    return await this.epoxRepository.delete(epox);
  }

  async convert(userId: ObjectID, convertEpoxDto: ConvertEpoxDto) {
    const epox: Epox = await this.epoxRepository.findOne(convertEpoxDto.id);
    if (!epox) {
      throw new BadRequestException('Epox could not found!');
    }

    if (!epox.userId.equals(userId)) {
      throw new BadRequestException('Epox does not belong to you!');
    }

    await this.epoxRepository.delete(epox);
    return await this.knowledgeObjectService.create(
      userId,
      new CreateKnowledgeObjectDto({
        title: convertEpoxDto.title,
        knowledge: convertEpoxDto.knowledge,
      }),
    );
  }
}
