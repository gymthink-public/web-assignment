import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class CreateEpoxDto {
  @ApiProperty()
  @IsNotEmpty()
  title: string;
}
