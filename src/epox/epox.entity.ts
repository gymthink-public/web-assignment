import { Entity, Column, ObjectIdColumn } from 'typeorm';
import { ObjectID } from 'mongodb';

@Entity()
export class Epox {
  @ObjectIdColumn()
  id: ObjectID;

  @Column()
  userId: ObjectID;

  @Column()
  title: string;

  constructor(param) {
    Object.assign(this, param);
  }
}
