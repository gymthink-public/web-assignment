import { Test, TestingModule } from '@nestjs/testing';
import { EpoxController } from './epox.controller';
import { EpoxService } from './epox.service';

describe('Epox Controller', () => {
  let controller: EpoxController;
  let epoxService: EpoxService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [EpoxController],
      providers: [{ provide: EpoxService, useValue: {} }],
    }).compile();

    controller = module.get<EpoxController>(EpoxController);
    epoxService = module.get<EpoxService>(EpoxService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
